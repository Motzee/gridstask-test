const path = require('path') ;

module.exports = {
    entry : './index.js',
    output : {
        path : path.resolve('./'),
        filename : 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
            ]
        }]
    }
}
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GridStack</title>

    <link rel="stylesheet" href="node_modules/gridstack/dist/gridstack.css"/>
    <link rel="stylesheet" href="styles.css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.0/lodash.min.js"></script>
    <script src="node_modules/gridstack/dist/gridstack.js"></script>
    <script src="node_modules/gridstack/dist/gridstack.jQueryUI.js"></script>

</head>
<body>
<div class="container-fluid">
    <h1>Serialization demo</h1>

    <div>
        <a class="btn btn-default" id="save-grid" href="#">Save Grid</a>
        <a class="btn btn-default" id="load-grid" href="#">Load Grid</a>
        <a class="btn btn-default" id="clear-grid" href="#">Clear Grid</a>
    </div>

    <br/>

    <div class="grid-stack">
    </div>

    <hr/>

    <textarea id="saved-data" cols="100" rows="20" readonly="readonly"></textarea>
</div>

<script src="index.js"></script>

</body>
</html>